from aoc2021.day2 import part1, part2

input_txt = [
    "forward 5",
    "down 5",
    "forward 8",
    "up 3",
    "down 8",
    "forward 2",
]


def test_part1():
    assert part1(input_txt) == 150


def test_part2():
    assert part2(input_txt) == 900
