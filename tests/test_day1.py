from aoc2021.day1 import part1, part2

input_txt = [
    "199",
    "200",
    "208",
    "210",
    "200",
    "207",
    "240",
    "269",
    "260",
    "263",
]


def test_part1():
    assert part1(input_txt) == 7


def test_part2():
    assert part2(input_txt) == 5
