from aoc2021.day6 import part1, part2

input_txt = ["3,4,3,1,2"]


def test_part1():
    assert part1(input_txt) == 5934


def test_part2():
    assert part2(input_txt) == 26984457539
