from aoc2021.day16 import part1, part2

input_txt1 = [
    ["8A004A801A8002F478"],
    ["620080001611562C8802118E34"],
    ["C0015000016115A2E0802F182340"],
    ["A0016C880162017C3686B18A3D4780"],
]

input_txt2 = [
    ["C200B40A82"],
    ["04005AC33890"],
    ["880086C3E88112"],
    ["CE00C43D881120"],
    ["D8005AC2A8F0"],
    ["F600BC2D8F"],
    ["9C005AC2F8F0"],
    ["9C0141080250320F1802104A08"],
]

expected1 = [16, 12, 23, 31]
expected2 = [3, 54, 7, 9, 1, 0, 0, 1]


def test_part1():
    for i, e in zip(input_txt1, expected1):
        assert part1(i) == e


def test_part2():
    for i, e in zip(input_txt2, expected2):
        assert part2(i) == e
