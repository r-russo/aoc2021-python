from aoc2021.day12 import part1, part2

input_txt = [
    [
        "start-A",
        "start-b",
        "A-c",
        "A-b",
        "b-d",
        "A-end",
        "b-end",
    ],
    [
        "dc-end",
        "HN-start",
        "start-kj",
        "dc-start",
        "dc-HN",
        "LN-dc",
        "HN-end",
        "kj-sa",
        "kj-HN",
        "kj-dc",
    ],
    [
        "fs-end",
        "he-DX",
        "fs-he",
        "start-DX",
        "pj-DX",
        "end-zg",
        "zg-sl",
        "zg-pj",
        "pj-he",
        "RW-he",
        "fs-DX",
        "pj-RW",
        "zg-RW",
        "start-pj",
        "he-WI",
        "zg-he",
        "pj-fs",
        "start-RW",
    ],
]

expected1 = [10, 19, 226]
expected2 = [36, 103, 3509]


def test_part1():
    for i, e in zip(input_txt, expected1):
        assert part1(i) == e


def test_part2():
    for i, e in zip(input_txt, expected2):
        assert part2(i) == e
