from pytest import mark

from aoc2021.day23 import part1, part2

input_txt = [
    "#############",
    "#...........#",
    "###B#C#B#D###",
    "  #A#D#C#A#",
    "  #########",
]


@mark.skip
def test_part1():
    assert part1(input_txt) == 12521


@mark.skip
def test_part2():
    assert part2(input_txt) == 44169
