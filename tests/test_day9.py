from aoc2021.day9 import part1, part2

input_txt = [
    "2199943210",
    "3987894921",
    "9856789892",
    "8767896789",
    "9899965678",
]


def test_part1():
    assert part1(input_txt) == 15


def test_part2():
    assert part2(input_txt) == 1134
