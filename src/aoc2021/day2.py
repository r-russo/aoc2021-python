def part1(input_txt: list[str]) -> int:
    pos = 0
    depth = 0
    for command in input_txt:
        match command.split(" "):
            case ["forward", x]:
                pos += int(x)
            case ["down", x]:
                depth += int(x)
            case ["up", x]:
                depth -= int(x)
            case _:
                print(f"Command not recognized {command}")

    return pos * depth

def part2(input_txt: list[str]) -> int:
    pos = 0
    depth = 0
    aim = 0
    for command in input_txt:
        match command.split(" "):
            case ["forward", x]:
                pos += int(x)
                depth += aim * int(x)
            case ["down", x]:
                aim += int(x)
            case ["up", x]:
                aim -= int(x)
            case _:
                print(f"Command not recognized {command}")

    return pos * depth
