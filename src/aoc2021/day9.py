from functools import reduce


def get_heightmap(input_txt: list[str]) -> list[list[int]]:
    heightmap = [[int(x) for x in line] for line in input_txt]
    heightmap.insert(0, [9 for _ in range(len(heightmap[0]))])
    heightmap.append([9 for _ in range(len(heightmap[0]))])
    for row in heightmap:
        row.insert(0, 9)
        row.append(9)

    return heightmap


def find_lowpoints(heightmap: list[list[int]]) -> list[tuple[int, int]]:
    lowpoints = []
    for i, row in enumerate(heightmap):
        if i == 0 or i == len(heightmap) - 1:
            continue
        for j, col in enumerate(row):
            if j == 0 or j == len(row) - 1:
                continue
            neighbours = (
                heightmap[i + 1][j],
                heightmap[i - 1][j],
                heightmap[i][j + 1],
                heightmap[i][j - 1],
            )

            for n in neighbours:
                if n <= col:
                    break
            else:
                lowpoints.append((i, j))

    return lowpoints


def get_basil_size(
    heightmap: list[list[int]],
    center_row: int,
    center_col: int,
    visited: set[tuple[int, int]],
) -> int:
    neighbours = (
        (center_row + 1, center_col),
        (center_row - 1, center_col),
        (center_row, center_col + 1),
        (center_row, center_col - 1),
    )

    size = 1
    visited.add((center_row, center_col))
    for i, j in neighbours:
        if (i, j) in visited or heightmap[i][j] == 9:
            visited.add((i, j))
            continue

        size += get_basil_size(heightmap, i, j, visited)

    return size


def part1(input_txt: list[str]) -> int:
    heightmap = get_heightmap(input_txt)
    lowpoints = find_lowpoints(heightmap)
    risk_level = 0
    for i, j in lowpoints:
        risk_level += heightmap[i][j] + 1
    return risk_level


def part2(input_txt: list[str]) -> int:
    heightmap = get_heightmap(input_txt)
    lowpoints = find_lowpoints(heightmap)
    sizes = []

    for i, j in lowpoints:
        sizes.append(get_basil_size(heightmap, i, j, set()))

    return reduce(lambda r, x: r * x, sorted(sizes)[-3:], 1)
