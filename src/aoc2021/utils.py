from pathlib import Path


def read_file(day: int) -> list[str]:
    fname = f"inputs/day{day}.txt"
    if not Path(fname).exists():
        raise FileNotFoundError(f"File {fname} not found!")

    with open(fname, "r") as f:
        input_txt = f.read().strip().splitlines()

    return input_txt
