from dataclasses import dataclass, field

HEX_TO_BIN = {
    "0": "0000",
    "1": "0001",
    "2": "0010",
    "3": "0011",
    "4": "0100",
    "5": "0101",
    "6": "0110",
    "7": "0111",
    "8": "1000",
    "9": "1001",
    "A": "1010",
    "B": "1011",
    "C": "1100",
    "D": "1101",
    "E": "1110",
    "F": "1111",
}

OPERATION = {
    0: lambda x, y: x + y,
    1: lambda x, y: x * y,
    2: min,
    3: max,
    5: lambda x, y: int(x > y),
    6: lambda x, y: int(x < y),
    7: lambda x, y: int(x == y),
}


@dataclass
class Packet:
    version: int
    type_id: int
    subpackets: list["Packet"] = field(default_factory=list)
    literal_value: int = 0


def get_packet_header(packet: str) -> tuple[int, int, str]:
    version = int(packet[:3], 2)
    type_id = int(packet[3:6], 2)
    content = packet[6:]

    return version, type_id, content


def parse(packet: str) -> tuple[Packet, str]:
    version, type_id, content = get_packet_header(packet)
    root_packet = Packet(version, type_id)
    if type_id == 4:
        ix = 0
        chunk = content[ix : ix + 5]
        literal_value = ""
        while chunk[0] == "1":
            literal_value += chunk[1:]
            ix += 5
            chunk = content[ix : ix + 5]
        literal_value += chunk[1:]
        root_packet.literal_value = int(literal_value, 2)
        content = content[ix + 5 :]
    else:
        length_type_id = content[0]
        if length_type_id == "0":
            subpacket_length = int(content[1:16], 2)
            next_packet = content[16 : 16 + subpacket_length]
            while next_packet:
                subpacket, next_packet = parse(next_packet)
                root_packet.subpackets.append(subpacket)
            content = content[16 + subpacket_length :]
        else:
            num_subpackets = int(content[1:12], 2)
            content = content[12:]
            for _ in range(num_subpackets):
                subpacket, content = parse(content)
                root_packet.subpackets.append(subpacket)

    return root_packet, content


def sum_version(packet: Packet) -> int:
    if not packet.subpackets:
        return packet.version

    result = packet.version
    for subpacket in packet.subpackets:
        result += sum_version(subpacket)

    return result


def do_operations(packet: Packet) -> int:
    if not packet.subpackets:
        return packet.literal_value

    result = do_operations(packet.subpackets[0])
    if len(packet.subpackets) > 1:
        for subpacket in packet.subpackets[1:]:
            result = OPERATION[packet.type_id](
                result, do_operations(subpacket)
            )

    return result


def part1(input_txt: list[str]) -> int:
    incoming = input_txt[0]
    content = "".join([HEX_TO_BIN[x] for x in incoming])
    packet, _ = parse(content)

    return sum_version(packet)


def part2(input_txt: list[str]) -> int:
    incoming = input_txt[0]
    content = "".join([HEX_TO_BIN[x] for x in incoming])
    packet, _ = parse(content)

    return do_operations(packet)
