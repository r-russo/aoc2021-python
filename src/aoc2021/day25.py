from copy import deepcopy


def step(state: list[list[str]]) -> tuple[list[list[str]], bool]:
    new_state = deepcopy(state)
    changed = False
    for i, line in enumerate(state):
        for j, col in enumerate(line):
            next_col = (j + 1) % len(line)
            if col == ">" and line[next_col] == ".":
                changed = True
                new_state[i][next_col] = col
                new_state[i][j] = "."

    middle_state = deepcopy(new_state)
    for i, line in enumerate(middle_state):
        for j, col in enumerate(line):
            next_row = (i + 1) % len(middle_state)
            if col == "v" and middle_state[next_row][j] == ".":
                changed = True
                new_state[next_row][j] = col
                new_state[i][j] = "."

    return new_state, changed


def part1(input_txt: list[str]) -> int:
    state = [list(x) for x in input_txt]
    i = 0
    while True:
        i += 1
        state, changed = step(state)

        if not changed:
            return i


def part2(_: list[str]) -> int:
    return 0
