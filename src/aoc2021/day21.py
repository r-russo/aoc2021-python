from functools import cache


class DeterministicDice:
    def __init__(self) -> None:
        self._n = 0

    def roll(self) -> list[int]:
        r = []
        for _ in range(3):
            self._n += 1
            self._n %= 100
            r.append(self._n)
        return r


def get_starting_pos(input_txt: list[str]) -> tuple[int, int]:
    p1 = int(input_txt[0].split(": ")[-1])
    p2 = int(input_txt[1].split(": ")[-1])

    return p1, p2


def part1(input_txt: list[str]) -> int:
    p = list(get_starting_pos(input_txt))
    s = [0, 0]
    dice = DeterministicDice()
    turn = 0
    n_rolls = 0

    while True:
        p[turn] += sum(dice.roll())
        n_rolls += 3
        p[turn] %= 10

        if p[turn] == 0:
            s[turn] += 10
        else:
            s[turn] += p[turn]

        if s[turn] >= 1000:
            break

        turn = 0 if turn else 1

    return n_rolls * s[0 if turn else 1]


@cache
def dirac_roll(
    pos: tuple[int, int],
    scores: tuple[int, int],
    turn: int,
) -> tuple[int, int]:
    w1, w2 = 0, 0
    rolls = [
        (1, 1, 1),
        (1, 1, 2),
        (1, 1, 3),
        (1, 2, 1),
        (1, 2, 2),
        (1, 2, 3),
        (1, 3, 1),
        (1, 3, 2),
        (1, 3, 3),
        (2, 1, 1),
        (2, 1, 2),
        (2, 1, 3),
        (2, 2, 1),
        (2, 2, 2),
        (2, 2, 3),
        (2, 3, 1),
        (2, 3, 2),
        (2, 3, 3),
        (3, 1, 1),
        (3, 1, 2),
        (3, 1, 3),
        (3, 2, 1),
        (3, 2, 2),
        (3, 2, 3),
        (3, 3, 1),
        (3, 3, 2),
        (3, 3, 3),
    ]
    for roll in rolls:
        p1, p2 = pos
        s1, s2 = scores
        winner = False
        if turn == 0:
            p1 += sum(roll)
            p1 %= 10
            if p1 == 0:
                s1 += 10
            else:
                s1 += p1

            if s1 >= 21:
                w1 += 1
                winner = True
        else:
            p2 += sum(roll)
            p2 %= 10
            if p2 == 0:
                s2 += 10
            else:
                s2 += p2

            if s2 >= 21:
                w2 += 1
                winner = True

        if not winner:
            new_turn = 0 if turn else 1
            ww1, ww2 = dirac_roll((p1, p2), (s1, s2), new_turn)
            w1 += ww1
            w2 += ww2

    return (w1, w2)


def part2(input_txt: list[str]) -> int:
    p = get_starting_pos(input_txt)
    s = (0, 0)

    w1, w2 = dirac_roll(p, s, 0)
    return max(w1, w2)
