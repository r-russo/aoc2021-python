from collections import defaultdict, deque, namedtuple
from heapq import heappop, heappush

Position = namedtuple("Position", ("x", "y"))


def parse(input_txt: list[str]) -> tuple[dict[Position, int], int, int]:
    grid = defaultdict(lambda: 99)
    for y, line in enumerate(input_txt):
        for x, c in enumerate(line):
            grid[Position(x, y)] = int(c)

    return grid, len(input_txt[0]), len(input_txt)


def dijkstra(grid: dict[Position, int], w: int, h: int) -> int:
    q: set[Position] = set()
    dist: dict[Position, float] = {}
    prev: dict[Position, Position | None] = {}

    start = Position(0, 0)
    end = Position(w - 1, h - 1)

    for node in grid:
        dist[node] = float("inf")
        prev[node] = None
        q.add(node)

    dist[start] = 0

    while q:
        u: Position | None = None
        min_dist = float("inf")
        for v in q:
            if dist[v] < min_dist:
                min_dist = dist[v]
                u = v
        if u is None:
            print("invalid u")
            continue
        q.remove(u)

        neighbours = (
            Position(u.x + 1, u.y),
            Position(u.x - 1, u.y),
            Position(u.x, u.y + 1),
            Position(u.x, u.y - 1),
        )

        for n in neighbours:
            if n.x >= 0 and n.y >= 0 and n.x < w and n.y < h:
                new_risk = dist[u] + grid[n]
                if new_risk < dist[n]:
                    dist[n] = new_risk
                    prev[n] = u

    return int(dist[end])


def bfs(grid: dict[Position, int], w: int, h: int) -> int:
    q: list[tuple[int, Position]] = []
    visited: set[Position] = set()

    destination = Position(w - 1, h - 1)
    heappush(q, (0, Position(0, 0)))
    visited.add(Position(0, 0))

    min_risk: int | None = None
    while q:
        risk, pos = heappop(q)

        if pos == destination:
            if min_risk is None:
                min_risk = risk
            else:
                min_risk = min(risk, min_risk)

        neighbours = (
            Position(pos.x + 1, pos.y),
            Position(pos.x - 1, pos.y),
            Position(pos.x, pos.y + 1),
            Position(pos.x, pos.y - 1),
        )

        for n in neighbours:
            if n.x >= 0 and n.y >= 0 and n.x < w and n.y < h:
                new_risk = risk + grid[n]
                if n in visited:
                    continue
                visited.add(n)
                heappush(q, (new_risk, n))

    if min_risk is not None:
        return min_risk
    return -1


def part1(input_txt: list[str]) -> int:
    grid, w, h = parse(input_txt)
    return bfs(grid, w, h)


def part2(input_txt: list[str]) -> int:
    grid, w, h = parse(input_txt)

    full_grid: dict[Position, int] = {}
    for pos in grid:
        for i in range(5):
            for j in range(5):
                new_pos = Position(pos.x + w * j, pos.y + h * i)
                new_risk = grid[pos] + i + j
                full_grid[new_pos] = new_risk % 10 + new_risk // 10

    w *= 5
    h *= 5

    return bfs(full_grid, w, h)
