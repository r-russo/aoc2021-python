from collections import Counter


def part1(input_txt: list[str]) -> int:
    l = len(input_txt[0])
    gamma = ""
    epsilon = ""
    for i in range(l):
        nums = Counter(line[i] for line in input_txt).most_common()
        gamma += max(nums[0][0])
        epsilon += min(nums[-1][0])

    return int(gamma, 2) * int(epsilon, 2)


def part2(input_txt: list[str]) -> int:
    l = len(input_txt[0])

    def get_rating(values: list[str], oxygen: bool = True) -> int:
        ix = 0 if oxygen else 1
        r = "1" if ix == 0 else "0"
        for i in range(l):
            most_common = Counter(line[i] for line in values).most_common()
            values = list(filter(lambda x: x[i] == most_common[ix][0], values))

            if len(values) == 2:
                if values[0][i + 1] == r:
                    return int(values[0], 2)
                elif values[1][i + 1] == r:
                    return int(values[1], 2)

        return 0

    oxygen_rating = get_rating(input_txt)
    co2_rating = get_rating(input_txt, False)

    return oxygen_rating * co2_rating
