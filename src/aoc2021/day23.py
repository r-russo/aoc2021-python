import heapq
from dataclasses import dataclass


@dataclass(frozen=True)
class Amphipod:
    name: str
    x: int
    y: int

    def __lt__(self, other) -> bool:
        return self.name < other.name

    def __le__(self, other) -> bool:
        return self.name <= other.name


COSTS = {"A": 1, "B": 10, "C": 100, "D": 1000}


def get_state(amphipods: list[Amphipod]) -> tuple[Amphipod, ...]:
    return tuple(amphipods)


def print_board(board: list[list[str]], amphipods: list[Amphipod]):
    for i, row in enumerate(board):
        for j, col in enumerate(row):
            for a in amphipods:
                if a.x == j and a.y == i:
                    print(a.name, end="")
                    break
            else:
                print(col, end="")
        print()


def check_goal(amphipods: list[Amphipod], stack_size: int) -> bool:
    for a in amphipods:
        if 2 <= a.y <= 1 + stack_size:
            if a.name == "A" and a.x == 3:
                continue
            elif a.name == "B" and a.x == 5:
                continue
            elif a.name == "C" and a.x == 7:
                continue
            elif a.name == "D" and a.x == 9:
                continue
            else:
                return False
        else:
            return False

    return True


def move(
    amphipods: list[Amphipod], stack_size
) -> list[tuple[int, Amphipod, Amphipod]]:
    possible_moves: list[tuple[int, Amphipod, Amphipod]] = []

    stacks: list[list[Amphipod]] = [[], [], [], []]
    hall: list[Amphipod] = []

    for a1 in amphipods:
        if a1.y == 1:
            hall.append(a1)
        elif a1.y > 1 and a1.x == 3:
            stacks[0].append(a1)
        elif a1.y > 1 and a1.x == 5:
            stacks[1].append(a1)
        elif a1.y > 1 and a1.x == 7:
            stacks[2].append(a1)
        elif a1.y > 1 and a1.x == 9:
            stacks[3].append(a1)

    for stack in stacks:
        stack.sort(key=lambda a: a.y)

    for a1 in hall:
        if a1.name == "A":
            target_x = 3
            target_stack = 0
        elif a1.name == "B":
            target_x = 5
            target_stack = 1
        elif a1.name == "C":
            target_x = 7
            target_stack = 2
        else:
            target_x = 9
            target_stack = 3

        for a2 in hall:
            if a1 == a2:
                continue

            if a1.x < a2.x < target_x or target_x < a2.x < a1.x:
                break
        else:
            stack = stacks[target_stack]
            if len(stack) == stack_size:
                continue
            for a2 in stack:
                if a2.name != a1.name:
                    break
            else:
                target_y = 1 + stack_size - len(stack)
                steps = abs(target_x - a1.x) + abs(a1.y - target_y)

                possible_moves.append(
                    (steps, a1, Amphipod(a1.name, target_x, target_y))
                )

    for i, stack in enumerate(stacks):
        ordered = True
        for a in stack:
            if a.name == "A":
                target_stack = 0
            elif a.name == "B":
                target_stack = 1
            elif a.name == "C":
                target_stack = 2
            else:
                target_stack = 3

            if i != target_stack:
                ordered = False
                break
        if not ordered:
            a1 = stack[0]
            target_x = [1, 2, 4, 6, 8, 10, 11]

            for a2 in hall:
                if a2.x < a1.x:
                    for i in range(len(target_x)):
                        if target_x[i] <= a2.x:
                            target_x[i] = -1
                elif a2.x > a1.x:
                    for i in range(len(target_x)):
                        if target_x[i] >= a2.x:
                            target_x[i] = -1

            for x in target_x:
                if x == -1:
                    continue
                steps = abs(a1.y - 1) + abs(a1.x - x)
                possible_moves.append((steps, a1, Amphipod(a1.name, x, 1)))

    return possible_moves


def find_cheapest_path(amphipods: list[Amphipod], stack_size: int = 2) -> int:
    q: list[tuple[int, list[Amphipod]]] = []
    visited: set[tuple[Amphipod, ...]] = set()
    visited.add(get_state(amphipods))
    heapq.heappush(q, (0, amphipods))
    while q:
        cost, a = heapq.heappop(q)

        if check_goal(a, stack_size):
            return cost

        for steps, old_a, new_a in move(a, stack_size):
            next_a = a.copy()
            next_a[a.index(old_a)] = new_a
            delta_cost = steps * COSTS[new_a.name]
            new_state = get_state(next_a)
            if new_state not in visited:
                if not check_goal(next_a, stack_size):
                    visited.add(new_state)
                heapq.heappush(q, (cost + delta_cost, next_a))

    return -1


def parse(input_txt: list[str]) -> tuple[list[list[str]], list[Amphipod]]:
    board: list[list[str]] = []
    amphipods: list[Amphipod] = []
    for i, line in enumerate(input_txt):
        row = list(line)
        for j, c in enumerate(line):
            if c.isalpha():
                amphipods.append(Amphipod(c, j, i))
                row[j] = "."
        board.append(row)

    return board, amphipods


def part1(input_txt: list[str]) -> int:
    _, amphipods = parse(input_txt)

    return find_cheapest_path(amphipods)


def part2(input_txt: list[str]) -> int:
    new_lines = [
        "  #D#C#B#A#",
        "  #D#B#A#C#",
    ]
    input_txt.insert(3, new_lines[0])
    input_txt.insert(4, new_lines[1])
    _, amphipods = parse(input_txt)
    return find_cheapest_path(amphipods, 4)
