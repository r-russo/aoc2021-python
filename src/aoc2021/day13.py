from collections import namedtuple

Point = namedtuple("Point", ["x", "y"])


def parse(input_txt: list[str]) -> tuple[set[Point], list[Point]]:
    grid: set[Point] = set()

    folds: list[Point] = []
    for line in input_txt:
        if not line:
            continue
        if "fold along" not in line:
            p = [int(x) for x in line.split(",")]
            grid.add(Point(p[0], p[1]))
        else:
            fold = line.split()[-1]
            num = int(fold.split("=")[-1])
            if fold[0] == "x":
                folds.append(Point(num, 0))
            elif fold[0] == "y":
                folds.append(Point(0, num))

    return grid, folds


def show_grid(grid: set[Point]):
    bottom_right = Point(0, 0)
    for k in grid:
        bottom_right = Point(
            max(k.x, bottom_right.x), max(k.y, bottom_right.y)
        )

    for y in range(bottom_right.y + 1):
        for x in range(bottom_right.x + 1):
            if Point(x, y) in grid:
                print("#", end="")
            else:
                print(".", end="")
        print()


def make_fold(grid: set[Point], fold: Point) -> set[Point]:
    new_grid: set[Point] = set()

    for point in grid:
        if fold.x > 0:
            if point.x < fold.x:
                new_grid.add(point)
            else:
                new_point = Point(2 * fold.x - point.x, point.y)
                new_grid.add(new_point)
        elif fold.y > 0:
            if point.y < fold.y:
                new_grid.add(point)
            else:
                new_point = Point(point.x, 2 * fold.y - point.y)
                new_grid.add(new_point)

    return new_grid


def part1(input_txt: list[str]) -> int:
    grid, folds = parse(input_txt)
    grid = make_fold(grid, folds[0])
    return len(grid)


def part2(input_txt: list[str]) -> int:
    grid, folds = parse(input_txt)
    for fold in folds:
        grid = make_fold(grid, fold)

    show_grid(grid)
    return 0
