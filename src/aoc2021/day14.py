from collections import Counter, defaultdict


def parse(input_txt: list[str]) -> tuple[dict[str, int], dict[str, str]]:
    template: str | None = None
    pairs: dict[str, str] = {}
    for line in input_txt:
        if line:
            if template is None:
                template = line
            else:
                pair = line.split(" -> ")
                pairs[pair[0]] = pair[1]

    if template is None:
        raise ValueError("Empty template")

    template_pairs: dict[str, int] = defaultdict(int)
    for c1, c2 in zip(template, template[1:]):
        pair = c1 + c2
        template_pairs[pair] += 1
    return template_pairs, pairs


def step(template: dict[str, int], pairs: dict[str, str]) -> dict[str, int]:
    new_template = defaultdict(int)
    for pair in template:
        if pair in pairs:
            c = pairs[pair]
            new_template[pair[0] + c] += template[pair]
            new_template[c + pair[1]] += template[pair]
        else:
            new_template[pair] += template[pair]
    return new_template


def part1(input_txt: list[str]) -> int:
    template, pairs = parse(input_txt)

    for _ in range(10):
        template = step(template, pairs)

    counts = defaultdict(int)
    for pair in template:
        counts[pair[0]] += template[pair]
    counts[input_txt[0][-1]] += 1

    return max(counts.values()) - min(counts.values())


def part2(input_txt: list[str]) -> int:
    template, pairs = parse(input_txt)

    for _ in range(40):
        template = step(template, pairs)

    counts = defaultdict(int)
    for pair in template:
        counts[pair[0]] += template[pair]
    counts[input_txt[0][-1]] += 1

    return max(counts.values()) - min(counts.values())
