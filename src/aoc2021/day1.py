def part1(input_txt: list[str]):
    depths = [int(x) for x in input_txt]
    return sum(d2 > d1 for d1, d2 in zip(depths[:-1], depths[1:]))


def part2(input_txt: list[str]):
    depths = [int(x) for x in input_txt]
    sliding_win = [
        (d1 + d2 + d3) / 3
        for d1, d2, d3 in zip(depths, depths[1:], depths[2:])
    ]
    return sum(d2 > d1 for d1, d2 in zip(sliding_win[:-1], sliding_win[1:]))
