from copy import deepcopy
from dataclasses import dataclass
from typing import Counter


@dataclass(frozen=True)
class Position:
    x: int
    y: int


class Image:
    def __init__(self, image: list[str]) -> None:
        self._i: list[list[int]] = []

        for line in image:
            self._i.append([1 if c == "#" else 0 for c in line])

    def width(self) -> int:
        return len(self._i[0])

    def height(self) -> int:
        return len(self._i)

    def size(self) -> tuple[int, int]:
        return self.width(), self.height()

    def _update_minmax(self, pos: Position):
        self.min = Position(min(self.min.x, pos.x), min(self.min.y, pos.y))
        self.max = Position(max(self.max.x, pos.x), max(self.max.y, pos.y))

    def set_px(self, pos: Position):
        self._i[pos.y][pos.x] = 1

    def clear_px(self, pos: Position):
        self._i[pos.y][pos.x] = 0

    def get_px(self, pos: Position) -> int:
        w, h = self.size()
        if pos.y < h and pos.x < w and pos.x >= 0 and pos.y >= 0:
            return self._i[pos.y][pos.x]

        x = min(max(0, pos.x), w - 1)
        y = min(max(0, pos.y), h - 1)

        return self._i[y][x]

    def grow(self):
        for row in self._i:
            row.append(0)
            row.insert(0, 0)
        self._i.insert(0, [0 for _ in self._i])
        self._i.append([0 for _ in self._i])

    def count_light_px(self) -> int:
        c = Counter()
        for row in self._i:
            c += Counter(row)
        return c[1]

    def __str__(self) -> str:
        r = ""
        w, h = self.size()
        for y in range(h):
            for x in range(w):
                px = self.get_px(Position(x, y))
                if px:
                    r += "#"
                else:
                    r += " "
            r += "\n"

        return r


def parse(input_txt: list[str]) -> tuple[str, Image]:
    parsing_algorithm = True
    algorithm = ""
    image: list[str] = []
    for line in input_txt:
        if not line:
            parsing_algorithm = False
            continue

        if parsing_algorithm:
            algorithm += line
        else:
            image.append(line)

    return algorithm, Image(image)


def apply_kernel(pos: Position, image: Image, output: Image, algorithm: str):
    kernel = (
        (-1, -1),
        (0, -1),
        (1, -1),
        (-1, 0),
        (0, 0),
        (1, 0),
        (-1, 1),
        (0, 1),
        (1, 1),
    )
    combined = ""
    for x, y in kernel:
        new_pos = Position(pos.x + x, pos.y + y)
        combined += str(image.get_px(new_pos))

    combined_dec = int(combined, 2)
    px = algorithm[combined_dec]
    if px == "#":
        output.set_px(pos)
    else:
        output.clear_px(pos)


def part1(input_txt: list[str]) -> int:
    algorithm, image = parse(input_txt)
    for _ in range(4):
        image.grow()

    for _ in range(2):
        output_image = deepcopy(image)
        w, h = image.size()
        for y in range(h):
            for x in range(w):
                apply_kernel(Position(x, y), image, output_image, algorithm)
        image = output_image

    return image.count_light_px()


def part2(input_txt: list[str]) -> int:
    algorithm, image = parse(input_txt)
    for _ in range(60):
        image.grow()

    for _ in range(50):
        output_image = deepcopy(image)
        w, h = image.size()
        for y in range(h):
            for x in range(w):
                apply_kernel(Position(x, y), image, output_image, algorithm)
        image = output_image

    return image.count_light_px()
