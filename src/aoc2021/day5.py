from collections import defaultdict, namedtuple

Coordinate = namedtuple("Coordinate", ("x", "y"))


def parse_input(input_txt: list[str]) -> list[tuple[Coordinate, Coordinate]]:
    coords = []
    for line in input_txt:
        c = line.split(" -> ")
        coords.append(
            (
                Coordinate(*[int(x) for x in c[0].split(",")]),
                Coordinate(*[int(x) for x in c[1].split(",")]),
            )
        )

    return coords


def sign(a):
    if a > 0:
        return 1
    elif a < 0:
        return -1
    else:
        return 0


def part1(input_txt: list[str]) -> int:
    coords = parse_input(input_txt)
    grid: dict[Coordinate, int] = defaultdict(int)
    for coord in coords:
        if not (coord[0].x == coord[1].x or coord[0].y == coord[1].y):
            continue

        if coord[0].x == coord[1].x:
            s = sign(coord[1].y - coord[0].y)
            for y in range(coord[0].y, coord[1].y + s, s):
                grid[Coordinate(coord[0].x, y)] += 1

        if coord[0].y == coord[1].y:
            s = sign(coord[1].x - coord[0].x)
            for x in range(coord[0].x, coord[1].x + s, s):
                grid[Coordinate(x, coord[0].y)] += 1

    return sum(v >= 2 for _, v in grid.items())


def part2(input_txt: list[str]) -> int:
    coords = parse_input(input_txt)
    grid: dict[Coordinate, int] = defaultdict(int)
    for coord in coords:
        if coord[0].x == coord[1].x:
            s = sign(coord[1].y - coord[0].y)
            for y in range(coord[0].y, coord[1].y + s, s):
                grid[Coordinate(coord[0].x, y)] += 1

        elif coord[0].y == coord[1].y:
            s = sign(coord[1].x - coord[0].x)
            for x in range(coord[0].x, coord[1].x + s, s):
                grid[Coordinate(x, coord[0].y)] += 1

        else:
            sx = sign(coord[1].x - coord[0].x)
            sy = sign(coord[1].y - coord[0].y)
            y = coord[0].y
            for x in range(coord[0].x, coord[1].x + sx, sx):
                grid[Coordinate(x, y)] += 1
                y += sy

    return sum(v >= 2 for _, v in grid.items())
