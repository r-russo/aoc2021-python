def simulate(initial_states: list[int], n_days: int) -> int:
    counts = [0 for _ in range(7)]
    new_fishes = [0, 0, 0]
    for s in initial_states:
        counts[s] += 1

    for day in range(n_days):
        current_ix = day % 7
        new_fishes[-1] += counts[current_ix]

        counts[current_ix] += new_fishes[0]
        new_fishes[:-1] = new_fishes[1:]
        new_fishes[-1] = 0

    return sum(counts) + sum(new_fishes)


def part1(input_txt: list[str]) -> int:
    states = [int(x) for x in input_txt[0].split(",")]

    return simulate(states, 80)


def part2(input_txt: list[str]) -> int:
    states = [int(x) for x in input_txt[0].split(",")]

    return simulate(states, 256)
