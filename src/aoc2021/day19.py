from __future__ import annotations

from collections import defaultdict
from dataclasses import dataclass, field
from math import sqrt
from typing import Counter


@dataclass(frozen=True)
class Vector:
    x: int
    y: int
    z: int

    def manhattan(self) -> int:
        return abs(self.x) + abs(self.y) + abs(self.z)

    def __add__(self, p: Vector) -> Vector:
        return Vector(self.x + p.x, self.y + p.y, self.z + p.z)

    def __sub__(self, p: Vector) -> Vector:
        return Vector(self.x - p.x, self.y - p.y, self.z - p.z)

    def __str__(self) -> str:
        return f"{self.x},{self.y},{self.z}"


@dataclass
class Scanner:
    beacons: list[Vector] = field(default_factory=list)

    def get_rotations(self) -> list[Scanner]:
        rotations = [Scanner() for _ in range(48)]
        for beacon in self.beacons:
            for i in range(8):
                x_sign = -1 if i & 0b100 else 1
                y_sign = -1 if i & 0b010 else 1
                z_sign = -1 if i & 0b001 else 1
                x = beacon.x * x_sign
                y = beacon.y * y_sign
                z = beacon.z * z_sign
                rotations[i].beacons.append(Vector(x, y, z))
                rotations[i + 8].beacons.append(Vector(z, x, y))
                rotations[i + 16].beacons.append(Vector(z, y, x))
                rotations[i + 24].beacons.append(Vector(x, z, y))
                rotations[i + 32].beacons.append(Vector(y, x, z))
                rotations[i + 40].beacons.append(Vector(y, z, x))

        return rotations

    def __str__(self) -> str:
        r = "--- scanner ---\n"
        for beacon in self.beacons:
            r += f"{beacon}\n"
        return r


def parse(input_txt: list[str]) -> list[Scanner]:
    scanners: list[Scanner] = []
    current_scanner = Scanner()
    for line in input_txt[1:]:
        if "scanner" in line:
            scanners.append(current_scanner)
            current_scanner = Scanner()
            continue

        if line:
            x, y, z = [int(p) for p in line.split(",")]
            current_scanner.beacons.append(Vector(x, y, z))
    scanners.append(current_scanner)

    return scanners


def map_scanners(scanners: list[Scanner]) -> tuple[Scanner, list[Vector]]:
    marked = [False for _ in scanners]
    s1 = scanners[0]
    positions = [Vector(0, 0, 0)]
    marked[0] = True
    i = 0

    while not all(marked):
        i += 1
        i %= len(scanners)

        if marked[i]:
            continue

        s2 = scanners[i]

        for rot in s2.get_rotations():
            matches: list[list[Vector]] = []
            for b1 in s1.beacons:
                curr = []
                for b2 in rot.beacons:
                    curr.append(b1 - b2)
                matches.append(curr)

            cnt = Counter()
            for m in matches:
                cnt += Counter(m)

            d, c = cnt.most_common()[0]
            if c >= 12:
                positions.append(d)
                for b in rot.beacons:
                    new_pos = Vector(b.x + d.x, b.y + d.y, b.z + d.z)
                    if new_pos not in s1.beacons:
                        s1.beacons.append(new_pos)
                    marked[i] = True
                # print(f"Marked {i}, remaining {len(marked) - sum(marked)}")
                break

    return s1, positions


def part1(input_txt: list[str]) -> int:
    scanners = parse(input_txt)
    return len(map_scanners(scanners)[0].beacons)


def part2(input_txt: list[str]) -> int:
    scanners = parse(input_txt)
    s, pos = map_scanners(scanners)
    max_dist = 0
    for s1 in pos:
        for s2 in pos:
            if s1 == s2:
                continue
            max_dist = max(max_dist, (s2 - s1).manhattan())

    return max_dist
