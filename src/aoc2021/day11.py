def parse(input_txt: list[str]) -> list[list[int]]:
    return [[int(x) for x in line] for line in input_txt]


def get_neighbours(i: int, j: int, w: int, h: int) -> list[tuple[int, int]]:
    neighbours: list[tuple[int, int]] = []
    if i + 1 < h:
        neighbours.append((i + 1, j))
        if j + 1 < w:
            neighbours.append((i + 1, j + 1))
        if j - 1 >= 0:
            neighbours.append((i + 1, j - 1))
    if i - 1 >= 0:
        neighbours.append((i - 1, j))
        if j + 1 < w:
            neighbours.append((i - 1, j + 1))
        if j - 1 >= 0:
            neighbours.append((i - 1, j - 1))
    if j + 1 < w:
        neighbours.append((i, j + 1))
    if j - 1 >= 0:
        neighbours.append((i, j - 1))

    return neighbours


def step(grid: list[list[int]]) -> int:
    flash: list[tuple[int, int]] = []
    h = len(grid)
    w = len(grid[0])
    for i in range(h):
        for j in range(w):
            grid[i][j] += 1
            if grid[i][j] > 9:
                grid[i][j] = 0
                flash.append((i, j))

    count = 0
    while flash:
        last = flash.pop()
        count += 1
        neighbours = get_neighbours(last[0], last[1], w, h)
        for i, j in neighbours:
            if grid[i][j] == 0:
                continue
            grid[i][j] += 1
            if grid[i][j] > 9:
                grid[i][j] = 0
                flash.append((i, j))

    return count


def part1(input_txt: list[str]) -> int:
    grid = parse(input_txt)
    count = 0
    for _ in range(100):
        count += step(grid)
    return count


def part2(input_txt: list[str]) -> int:
    grid = parse(input_txt)
    n_step = 0
    while True:
        count = step(grid)
        n_step += 1
        if count == len(grid) * len(grid[0]):
            return n_step
