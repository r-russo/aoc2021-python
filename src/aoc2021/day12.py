from collections import defaultdict, deque


def parse(input_txt: list[str]) -> dict[str, list[str]]:
    graph: dict[str, list[str]] = defaultdict(list)
    for line in input_txt:
        nodes = line.split("-")
        for start in nodes:
            for node in nodes:
                if start == node:
                    continue
                if node not in graph[start]:
                    graph[start].append(node)

    return graph


def bfs(graph: dict[str, list[str]]) -> list[str]:
    queue: deque[tuple[str, str]] = deque()
    queue.append(("start", "start"))
    paths: list[str] = []

    while queue:
        v, path = queue.pop()
        if v == "end":
            paths.append(path)
            continue

        for node in graph[v]:
            if node == node.lower() and node in path:
                continue
            new_path = path + f",{node}"
            queue.appendleft((node, new_path))

    return paths


correct_paths = [
    "start,A,b,A,b,A,c,A,end",
    "start,A,b,A,b,A,end",
    "start,A,b,A,b,end",
    "start,A,b,A,c,A,b,A,end",
    "start,A,b,A,c,A,b,end",
    "start,A,b,A,c,A,c,A,end",
    "start,A,b,A,c,A,end",
    "start,A,b,A,end",
    "start,A,b,d,b,A,c,A,end",
    "start,A,b,d,b,A,end",
    "start,A,b,d,b,end",
    "start,A,b,end",
    "start,A,c,A,b,A,b,A,end",
    "start,A,c,A,b,A,b,end",
    "start,A,c,A,b,A,c,A,end",
    "start,A,c,A,b,A,end",
    "start,A,c,A,b,d,b,A,end",
    "start,A,c,A,b,d,b,end",
    "start,A,c,A,b,end",
    "start,A,c,A,c,A,b,A,end",
    "start,A,c,A,c,A,b,end",
    "start,A,c,A,c,A,end",
    "start,A,c,A,end",
    "start,A,end",
    "start,b,A,b,A,c,A,end",
    "start,b,A,b,A,end",
    "start,b,A,b,end",
    "start,b,A,c,A,b,A,end",
    "start,b,A,c,A,b,end",
    "start,b,A,c,A,c,A,end",
    "start,b,A,c,A,end",
    "start,b,A,end",
    "start,b,d,b,A,c,A,end",
    "start,b,d,b,A,end",
    "start,b,d,b,end",
    "start,b,end",
]


def bfs2(graph: dict[str, list[str]]) -> list[str]:
    queue: deque[tuple[str, str, bool]] = deque()
    queue.appendleft(("start", "start", False))
    paths: list[str] = []

    while queue:
        v, path, twice = queue.pop()
        if v == "end":
            paths.append(path)
            continue

        for node in graph[v]:
            new_twice = twice
            if node == "start":
                continue
            if node == node.lower() and node in path:
                if not twice:
                    new_twice = True
                else:
                    continue
            new_path = path + f",{node}"
            queue.appendleft((node, new_path, new_twice))

    return paths


def part1(input_txt: list[str]) -> int:
    graph = parse(input_txt)
    return len(bfs(graph))


def part2(input_txt: list[str]) -> int:
    graph = parse(input_txt)
    return len(bfs2(graph))
