def parse_input(
    input_txt: list[str],
) -> tuple[list[list[str]], list[list[str]]]:
    signals: list[list[str]] = []
    outputs: list[list[str]] = []
    for line in input_txt:
        entry = line.split(" | ")
        signals.append(entry[0].split())
        outputs.append(entry[1].split())

    return signals, outputs


def get_mappings(signal: list[str]) -> dict[str, int]:
    sorted_signals = ["".join(sorted(s)) for s in signal]
    mapping: dict[str, int] = {}
    reverse_mapping: dict[int, str] = {}

    groups_of_five: list[str] = []
    groups_of_six: list[str] = []
    for s in sorted_signals:
        if len(s) == 2:
            mapping[s] = 1
            reverse_mapping[1] = s
        elif len(s) == 3:
            mapping[s] = 7
            reverse_mapping[7] = s
        elif len(s) == 4:
            mapping[s] = 4
            reverse_mapping[4] = s
        elif len(s) == 5:
            groups_of_five.append(s)
        elif len(s) == 6:
            groups_of_six.append(s)
        elif len(s) == 7:
            mapping[s] = 8
            reverse_mapping[8] = s

    for s in groups_of_five:
        if set(reverse_mapping[1]).issubset(s):
            mapping[s] = 3
            reverse_mapping[3] = s
            break

    for s in groups_of_six:
        if set(reverse_mapping[4]).issubset(s):
            mapping[s] = 9
            reverse_mapping[9] = s
            break

    for s in groups_of_five:
        if s not in mapping and set(s).issubset(reverse_mapping[9]):
            mapping[s] = 5
            reverse_mapping[5] = s
            break

    for s in groups_of_six:
        if s not in mapping and set(reverse_mapping[5]).issubset(s):
            mapping[s] = 6
            reverse_mapping[6] = s
            break

    for s in groups_of_five:
        if s not in mapping:
            mapping[s] = 2
            reverse_mapping[2] = s
            break

    for s in groups_of_six:
        if s not in mapping:
            mapping[s] = 0
            reverse_mapping[0] = s
            break

    return mapping


def part1(input_txt: list[str]) -> int:
    _, outputs = parse_input(input_txt)
    count = 0
    for output in outputs:
        for segments in output:
            if len(segments) in set((2, 3, 4, 7)):
                count += 1

    return count


def part2(input_txt: list[str]) -> int:
    signals, outputs = parse_input(input_txt)
    result = 0

    for signal, output in zip(signals, outputs):
        mapping = get_mappings(signal)

        num = 0
        for o in output:
            num *= 10
            num += mapping["".join(sorted(o))]

        result += num

    return result
