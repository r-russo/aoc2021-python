def parse_line(line: str) -> tuple[list[int], list[int]]:
    content = line[1:-1]

    nums: list[int] = []
    depths: list[int] = []

    depth = 0
    for i, c in enumerate(content):
        if c == "[":
            depth += 1
        elif c == "]":
            depth -= 1
        elif c == ",":
            continue
        elif c.isdigit():
            nums.append(int(c))
            depths.append(depth)

    return nums, depths


def get_number(nums: list[int], depths: list[int]) -> str:
    last_depth = 0
    num = "["
    for i, depth in enumerate(depths):
        delta = depth - last_depth
        if delta > 0:
            num += "[" * delta
        elif delta < 0:
            num += "]" * (-delta + 1)
            num += "[" * depth

        # if depth == 1:
        #     num += "["
        if i > 0:
            num += ","
        num += str(nums[i])
        last_depth = depth
    num += "]" * (last_depth + 1)
    return num


def explode(nums: list[int], depths: list[int]) -> bool:
    changed = False
    while 4 in depths:
        changed = True
        ix = depths.index(4)
        if ix > 0:
            nums[ix - 1] += nums[ix]
        if ix < len(depths) - 2:
            nums[ix + 2] += nums[ix + 1]

        del nums[ix + 1]
        del depths[ix + 1]
        nums[ix] = 0
        depths[ix] -= 1

    return changed


def split(nums: list[int], depths: list[int]) -> bool:
    changed = False
    for i, num in enumerate(nums):
        if num >= 10:
            changed = True
            left = num // 2
            if num % 2 == 0:
                right = left
            else:
                right = left + 1
            nums[i] = left
            depths[i] += 1
            nums.insert(i + 1, right)
            depths.insert(i + 1, depths[i])
            break

    return changed


def reduce(nums: list[int], depths: list[int]):
    done = False
    while not done:
        changed = explode(nums, depths)
        changed |= split(nums, depths)
        done = not changed


def magnitude(nums: list[int], depths: list[int]) -> int:
    max_depth = max(depths)
    for depth in range(max_depth, -1, -1):
        while depth in depths:
            ix = depths.index(depth)
            nums[ix] = 3 * nums[ix] + 2 * nums[ix + 1]
            depths[ix] -= 1
            del nums[ix + 1]
            del depths[ix + 1]
    return nums[0]


def part1(input_txt: list[str]) -> int:
    nums, depths = parse_line(input_txt[0])
    for line in input_txt[1:]:
        n, d = parse_line(line)
        nums.extend(n)
        depths.extend(d)
        depths = [d + 1 for d in depths]
        done = reduce(nums, depths)
        while done:
            done = reduce(nums, depths)

    return magnitude(nums, depths)


def part2(input_txt: list[str]) -> int:
    max_mag = 0
    for line1 in input_txt:
        n, d = parse_line(line1)
        for line2 in input_txt:
            if line1 == line2:
                continue

            nums, depths = parse_line(line2)
            nums.extend(n)
            depths.extend(d)
            depths = [d + 1 for d in depths]
            done = reduce(nums, depths)
            while done:
                done = reduce(nums, depths)

            max_mag = max(max_mag, magnitude(nums, depths))

    return max_mag
