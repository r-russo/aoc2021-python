from math import inf


def fuel_cost(starting_pos: list[int], new_pos: int) -> int:
    fuel = 0
    for pos in starting_pos:
        fuel += abs(pos - new_pos)
    return fuel


def fuel_cost2(starting_pos: list[int], new_pos: int) -> int:
    fuel = 0
    for pos in starting_pos:
        distance = abs(pos - new_pos)
        fuel += distance * (distance + 1) // 2
    return fuel


def part1(input_txt: list[str]) -> int:
    crabs_pos = [int(pos) for pos in input_txt[0].split(",")]

    min_pos = min(crabs_pos)
    max_pos = max(crabs_pos)

    best = fuel_cost(crabs_pos, min_pos)
    for pos in range(min_pos + 1, max_pos + 1):
        best = min(fuel_cost(crabs_pos, pos), best)
    return best


def part2(input_txt: list[str]) -> int:
    crabs_pos = [int(pos) for pos in input_txt[0].split(",")]

    min_pos = min(crabs_pos)
    max_pos = max(crabs_pos)

    best = fuel_cost2(crabs_pos, min_pos)
    for pos in range(min_pos + 1, max_pos + 1):
        best = min(fuel_cost2(crabs_pos, pos), best)
    return best
