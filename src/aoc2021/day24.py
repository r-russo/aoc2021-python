from random import randint

Registers = dict[str, int]


def convert_to_value(b: str, regs: Registers) -> int:
    if b.lstrip("-").isdigit():
        return int(b)
    return regs[b]


def decompiled(inputs: list[int]) -> int:
    w, x, y, z = 0, 0, 0, 0
    sum_x = (14, 13, 13, 12, -12, 12, -2, -11, 13, 14, 0, -12, -13, -6)
    sum_y = ( 8,  8,  3, 10,   8,  8,  8,   5,  9,  3, 4,   9,   2,  7)
    div_z = ( 1,  1,  1,  1,  26,  1, 26,  26,  1,  1, 26, 26,  26, 26)
    for i, w in enumerate(inputs[::-1]):
        x = z % 26 + sum_x[i]
        y = 25 * (0 if x == w else 1) + 1
        z = z // div_z[i] * y + (w + sum_y[i]) * (0 if x == w else 1)

    return z


def alu(program: list[str], inputs: list[int]) -> Registers:
    regs: Registers = {"w": 0, "x": 0, "y": 0, "z": 0}

    for i, instruction in enumerate(program):
        match instruction.split():
            case "inp", a:
                # print(f"{a} = i[{14 - len(inputs)}]")
                if len(inputs) == 0:
                    # print(f"halted at line {i}: {instruction}")
                    return regs
                else:
                    regs[a] = inputs.pop()
            case "add", a, b:
                regs[a] += convert_to_value(b, regs)
                # print(f"{a} = {a} + {b}")
            case "mul", a, b:
                regs[a] *= convert_to_value(b, regs)
                # print(f"{a} = {a} * {b}")
            case "div", a, b:
                regs[a] //= convert_to_value(b, regs)
                # print(f"{a} = {a} / {b}")
            case "mod", a, b:
                regs[a] %= convert_to_value(b, regs)
                # print(f"{a} = {a} % {b}")
            case "eql", a, b:
                regs[a] = 1 if regs[a] == convert_to_value(b, regs) else 0
                # print(f"{a} = {a} == {b}")

    return regs


def part1(input_txt: list[str]) -> int:
    digits = list("1" * 14)
    digits[0] = str(9 - 2)
    digits[1] = str(9)
    digits[2] = str(9)
    digits[3] = str(9)
    digits[4] = str(int(digits[3]) - 2)
    digits[5] = str(9 - 6)
    digits[6] = str(int(digits[5]) + 6)
    digits[7] = str(int(digits[2]) - 8)
    digits[8] = str(9)
    digits[9] = str(9 - 3)
    digits[10] = str(int(digits[9]) + 3)
    digits[11] = str(int(digits[8]) - 3)
    digits[12] = str(int(digits[1]) - 5)
    digits[13] = str(int(digits[0]) + 2)

    num = [int(x) for x in digits[::-1]]
    if alu(input_txt, num.copy())["z"] == 0:
        return int("".join([str(x) for x in num[::-1]]))
    return -1



def part2(input_txt: list[str]) -> int:
    digits = list("1" * 14)
    digits[0] = str(3 - 2)
    digits[1] = str(6)
    digits[2] = str(9)
    digits[3] = str(3)
    digits[5] = str(7 - 6)
    digits[8] = str(4)
    digits[9] = str(4 - 3)

    digits[4] = str(int(digits[3]) - 2)
    digits[6] = str(int(digits[5]) + 6)
    digits[7] = str(int(digits[2]) - 8)
    digits[10] = str(int(digits[9]) + 3)
    digits[11] = str(int(digits[8]) - 3)
    digits[12] = str(int(digits[1]) - 5)
    digits[13] = str(int(digits[0]) + 2)

    num = [int(x) for x in digits[::-1]]
    if alu(input_txt, num.copy())["z"] == 0:
        return int("".join([str(x) for x in num[::-1]]))
    return -1
