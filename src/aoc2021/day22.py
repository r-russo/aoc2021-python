from collections import namedtuple

Point = namedtuple("Point", ("x", "y", "z"))
Cuboid = tuple[Point, Point, bool]


def intersect(c1: Cuboid, c2: Cuboid) -> Cuboid | None:
    cmin = Point(
        max(c1[0].x, c2[0].x),
        max(c1[0].y, c2[0].y),
        max(c1[0].z, c2[0].z),
    )

    cmax = Point(
        min(c1[1].x, c2[1].x),
        min(c1[1].y, c2[1].y),
        min(c1[1].z, c2[1].z),
    )

    for p1, p2 in zip(cmin, cmax):
        if p2 < p1:
            return None

    return (cmin, cmax, c2[2])


def parse(input_txt: list[str]) -> list[Cuboid]:
    cuboids: list[Cuboid] = []

    for line in input_txt:
        state, ranges = line.split()

        xrange, yrange, zrange = ranges.split(",")
        x = [int(x) for x in xrange[2:].split("..")]
        y = [int(y) for y in yrange[2:].split("..")]
        z = [int(z) for z in zrange[2:].split("..")]

        cuboids.append(
            (Point(x[0], y[0], z[0]), Point(x[1], y[1], z[1]), state == "on")
        )

    return cuboids


def count_cubes(cuboid: Cuboid) -> int:
    cmin, cmax, _ = cuboid
    if cmin.x > cmax.x or cmin.y > cmax.y or cmin.z > cmax.z:
        return 0
    return (
        (cmax.x - cmin.x + 1) * (cmax.y - cmin.y + 1) * (cmax.z - cmin.z + 1)
    )


def count_total_cubes(cuboids: list[Cuboid]) -> int:
    checked: list[Cuboid] = [cuboids[0]]
    for c1 in cuboids[1:]:
        for c2 in checked.copy():
            i = intersect(c1, c2)
            if i is not None:
                checked.append((i[0], i[1], not c2[2]))
        if c1[2]:
            checked.append(c1)

    return sum((count_cubes(c) if c[2] else -count_cubes(c) for c in checked))


def part1(input_txt: list[str]) -> int:
    cuboids = parse(input_txt)
    limited_cuboids: list[Cuboid] = []
    for cmin, cmax, state in cuboids:
        limited_cuboids.append(
            (
                Point(
                    max(-50, cmin.x),
                    max(-50, cmin.y),
                    max(-50, cmin.z),
                ),
                Point(
                    min(50, cmax.x),
                    min(50, cmax.y),
                    min(50, cmax.z),
                ),
                state,
            )
        )
    return count_total_cubes(limited_cuboids)


def part2(input_txt: list[str]) -> int:
    cuboids = parse(input_txt)
    return count_total_cubes(cuboids)
