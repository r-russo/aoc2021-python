CHARS = {"{": "}", "(": ")", "[": "]", "<": ">"}
POINTS1 = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}
POINTS2 = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
}


def parse(line: str) -> tuple[int, list[str] | None]:
    stack: list[str] = []
    for c in line:
        if c in CHARS.keys():  # opening:
            stack.append(c)
        elif c == CHARS[stack[-1]]:  # closing
            stack.pop()
        else:
            return POINTS1[c], None

    return 0, stack


def part1(input_txt: list[str]) -> int:
    result = 0
    for line in input_txt:
        result += parse(line)[0]

    return result


def part2(input_txt: list[str]) -> int:
    scores = []
    for line in input_txt:
        _, stack = parse(line)
        if stack is not None:
            score = 0
            while stack:
                last = stack.pop()
                c = CHARS[last]
                score *= 5
                score += POINTS2[c]

            scores.append(score)

    return sorted(scores)[len(scores) // 2]
