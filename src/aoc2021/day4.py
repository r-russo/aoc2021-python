class Board:
    def __init__(self, rows: list[str]) -> None:
        self._rows = [[int(x) for x in row.split()] for row in rows]
        self._marked = [[False for _ in row] for row in self._rows]
        self._bingo = False

    def mark(self, num: int) -> None:
        for i, row in enumerate(self._rows):
            if num in row:
                j = row.index(num)
                self._marked[i][j] = True

    def bingo(self) -> bool:
        if self._bingo:
            return True

        for i, row in enumerate(self._marked):
            if all(row):
                return True

            if all(self._marked[j][i] for j in range(len(self._marked))):
                self._bingo = True
                return True

        return False

    def score(self) -> int:
        result = 0
        for i, row in enumerate(self._rows):
            ixs = [j for j, m in enumerate(self._marked[i]) if not m]
            result += sum(row[j] for j in ixs)

        return result


def parse_input(input_txt: list[str]) -> tuple[list[int], list[Board]]:
    numbers = [int(x) for x in input_txt[0].split(",")]

    boards: list[Board] = []
    current_rows: list[str] = []
    for line in input_txt[2:]:
        if line == "" and current_rows:
            boards.append(Board(current_rows))
            current_rows = []
            continue

        current_rows.append(line)

    if current_rows:
        boards.append(Board(current_rows))

    return numbers, boards


def part1(input_txt: list[str]) -> int:
    numbers, boards = parse_input(input_txt)

    for draw in numbers:
        for board in boards:
            board.mark(draw)
            if board.bingo():
                return draw * board.score()

    return -1


def part2(input_txt: list[str]) -> int:
    numbers, boards = parse_input(input_txt)

    winner_boards: set[int] = set()
    for draw in numbers:
        for i, board in enumerate(boards):
            board.mark(draw)
            if board.bingo() and i not in winner_boards:
                winner_boards.add(i)
                if len(winner_boards) == len(boards):
                    return draw * board.score()

    return -1
