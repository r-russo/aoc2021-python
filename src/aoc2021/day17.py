from dataclasses import dataclass
from math import sqrt


@dataclass(frozen=True)
class Point:
    x: int
    y: int


@dataclass(frozen=True)
class Target:
    top_left: Point
    bottom_right: Point

    def has(self, p: Point) -> bool:
        return (
            p.x >= self.top_left.x
            and p.y >= self.top_left.y
            and p.x <= self.bottom_right.x
            and p.y <= self.bottom_right.y
        )

    def passed(self, p: Point) -> bool:
        return p.x > self.bottom_right.x or p.y < self.bottom_right.y


def get_target(target_area: str) -> Target:
    range = target_area.split(": ")[-1].split(", ")
    x_range = range[0].lstrip("x=").split("..")
    y_range = range[1].lstrip("y=").split("..")

    top_left = Point(int(x_range[0]), int(y_range[0]))
    bottom_right = Point(int(x_range[1]), int(y_range[1]))

    return Target(top_left, bottom_right)


def check_target(velocity: Point, target: Target) -> bool:
    b = 2 * velocity.y + 1
    for y in range(target.top_left.y, target.bottom_right.y + 1):
        tf = (b + sqrt(b ** 2 - 8 * y)) / 2
        if tf == int(tf):
            max_tf_vx = velocity.x
            if tf < max_tf_vx:
                x = velocity.x * tf - tf * (tf - 1) / 2
            else:
                x = velocity.x * max_tf_vx - max_tf_vx * (max_tf_vx - 1) / 2

            if x == int(x) and target.has(Point(int(x), y)):
                return True

    return False


def get_max_y(velocity: Point) -> int:
    tmax = velocity.y
    return int(velocity.y * tmax - tmax * (tmax - 1) / 2)


def part1(input_txt: list[str]) -> int:
    target = get_target(input_txt[0])
    max_y = 0

    for y in range(300):
        for x in range(target.top_left.x):
            velocity = Point(x, y)
            if check_target(velocity, target):
                max_y = max(max_y, get_max_y(velocity))

    return max_y


def part2(input_txt: list[str]) -> int:
    target = get_target(input_txt[0])
    c = 0
    for y in range(-300, 300):
        for x in range(target.bottom_right.x + 1):
            velocity = Point(x, y)
            if check_target(velocity, target):
                c += 1
    return c
