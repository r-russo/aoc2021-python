from typing import Any

from PIL import Image, ImageColor, ImageDraw

from aoc2021 import day11

INPUT = [
    "5483143223",
    "2745854711",
    "5264556173",
    "6141336146",
    "6357385478",
    "4167524645",
    "2176841721",
    "6882881134",
    "4846848554",
    "5283751526",
]

# MAP_COLORS = [((x + 1) * 10, (x + 1) * 10, x * 4) for x in range(10)]
MAP_COLORS = [(60, 80, x * 10) for x in range(10)]
MAP_COLORS[0] = (60, 80, 100)


def convert_grid_to_image(grid: list[list[int]], w: int, h: int) -> Any:
    grid_h = len(grid)
    grid_w = len(grid[0])
    light_w, light_h = (w // grid_w, h // grid_h)
    im = Image.new("RGB", (w, h))
    draw = ImageDraw.Draw(im)
    for i in range(grid_h):
        for j in range(grid_w):
            c = MAP_COLORS[grid[i][j]]
            draw.ellipse(
                [
                    (j * light_w, i * light_h),
                    ((j + 1) * light_w, (i + 1) * light_h),
                ],
                f"hsv({c[0]},{c[1]}%,{c[2]}%)",
            )
    return im


def main():
    grid = day11.parse(INPUT)
    w, h = 512, 512
    animation: list[Any] = []
    animation.append(convert_grid_to_image(grid, w, h))
    step = 0
    while True:
        step += 1
        count = day11.step(grid)
        animation.append(convert_grid_to_image(grid, w, h))
        if count == len(grid) * len(grid[0]):
            for _ in range(100):
                animation.append(convert_grid_to_image(grid, w, h))
            break

    animation[0].save(
        "day11_out.gif",
        save_all=True,
        append_images=animation[1:],
        duration=20,
    )


if __name__ == "__main__":
    main()
