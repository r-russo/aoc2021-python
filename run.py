import argparse
from time import perf_counter

from aoc2021.utils import read_file


def main():
    parser = argparse.ArgumentParser(description="Solve day n of AoC 2021")
    parser.add_argument("days", metavar="n", type=int, nargs="*")
    args = parser.parse_args()
    days = args.days

    if not days:
        days = list(range(1, 25))

    for day in days:
        input_txt = read_file(day)
        if day == 1:
            from aoc2021.day1 import part1, part2
        elif day == 2:
            from aoc2021.day2 import part1, part2
        elif day == 3:
            from aoc2021.day3 import part1, part2
        elif day == 4:
            from aoc2021.day4 import part1, part2
        elif day == 5:
            from aoc2021.day5 import part1, part2
        elif day == 6:
            from aoc2021.day6 import part1, part2
        elif day == 7:
            from aoc2021.day7 import part1, part2
        elif day == 8:
            from aoc2021.day8 import part1, part2
        elif day == 9:
            from aoc2021.day9 import part1, part2
        elif day == 10:
            from aoc2021.day10 import part1, part2
        elif day == 11:
            from aoc2021.day11 import part1, part2
        elif day == 12:
            from aoc2021.day12 import part1, part2
        elif day == 13:
            from aoc2021.day13 import part1, part2
        elif day == 14:
            from aoc2021.day14 import part1, part2
        elif day == 15:
            from aoc2021.day15 import part1, part2
        elif day == 16:
            from aoc2021.day16 import part1, part2
        elif day == 17:
            from aoc2021.day17 import part1, part2
        elif day == 18:
            from aoc2021.day18 import part1, part2
        elif day == 19:
            from aoc2021.day19 import part1, part2
        elif day == 20:
            from aoc2021.day20 import part1, part2
        elif day == 21:
            from aoc2021.day21 import part1, part2
        elif day == 22:
            from aoc2021.day22 import part1, part2
        elif day == 23:
            from aoc2021.day23 import part1, part2
        elif day == 24:
            from aoc2021.day24 import part1, part2
        else:
            print(f"Day {day} not solved")

        print(f"Solution for day {day} is")
        start = perf_counter()
        part1 = part1(input_txt)
        time_part1 = (perf_counter() - start) * 1000
        print(f"Part 1: {part1} (time elapsed: {time_part1:.3f} ms)")
        start = perf_counter()
        part2 = part2(input_txt)
        time_part2 = (perf_counter() - start) * 1000
        print(f"Part 2: {part2} (time elapsed: {time_part2:.3f} ms)")


if __name__ == "__main__":
    main()
